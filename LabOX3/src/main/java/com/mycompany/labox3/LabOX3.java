/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.labox3;

import java.util.Scanner;

/**
 *
 * @author Pee
 */
public class LabOX3 {

    static String[][] table = {{"-","-","-"},{"-","-","-"},{"-","-","-"}};
    static  String currentPlayer = "X";
    static int row, col;
    
    public static void main(String[] args) {
        printWelcome();
        while(true) {
            printTable();
            printTurn();
            inputRowCol();
            if(OX.checkWin(table, currentPlayer)) {
                if(OX.checkDraw(table, currentPlayer)) {
                    printTable();
                    printDraw();
                    break;
                }
                printTable();
                printWin();
                break;
            }
            switchPlayer();
        }
    }
    
    //Print Welcome message
    private static void printWelcome() {
        System.out.println("Welcome to OX");
    }
    
    //Create and print table
    private static void printTable() {
        for(int i=0; i<3; i++) {
            for(int j=0;j<3;j++) {
                System.out.print(table[i][j] +" ");
            }
            System.out.println("");
        }
    }
    
    //Show player turn
    private static void printTurn() {
        System.out.println("Player "+currentPlayer+" turn");
    }
    
    //Input row, col
    private static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while(true) {
            System.out.print("Please input row, col: ");
            row = kb.nextInt();
            col = kb.nextInt();
            if(table[row - 1][col - 1] == "-") {
                table[row - 1][col - 1] = currentPlayer;
                return;
            }
        }
    }
    
    //Switch player turn
    private static void switchPlayer() {
        if(currentPlayer=="X") {
            currentPlayer = "O";
        } else {
            currentPlayer = "X";
        }
    }
    
    //Print Winner
    private static void printWin() {
        System.out.println(currentPlayer +" Win!!");
    }
    
    //Print Draw
    private static void printDraw() {
        System.out.println("Game is draw!!");
    }
}
